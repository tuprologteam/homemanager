package model.agent;

public interface StateChangeListener {
	
	public void onStateChanged();
	
}
