package model.agent;

public interface RecipesChangeListener {
	
	public void onRecipesChanged();
	
}
