package it.unibo.homemanager.agents.wardrobe;

public interface ContentChangeListener {
	public void onContentChanged();

}

