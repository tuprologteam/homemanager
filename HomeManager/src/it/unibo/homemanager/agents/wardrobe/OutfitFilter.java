package it.unibo.homemanager.agents.wardrobe;

public interface OutfitFilter {
public boolean filter(Outfit o);
}
