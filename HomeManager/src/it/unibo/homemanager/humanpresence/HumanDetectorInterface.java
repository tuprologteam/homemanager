package it.unibo.homemanager.humanpresence;

public interface HumanDetectorInterface {

	public boolean isHumanPresent();
}
